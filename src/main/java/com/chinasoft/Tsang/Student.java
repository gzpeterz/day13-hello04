package com.chinasoft.Tsang;

import lombok.Data;

@Data
public class Student {

    private String id;   // add by teacher

    private String name;

    private int age;
}
