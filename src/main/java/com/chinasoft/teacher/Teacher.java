package com.chinasoft.teacher;

import lombok.Data;

/**
 * @author 中软zhang
 * @create 2023-07-31 9:43
 */
@Data
public class Teacher {

    Integer id;
    String name;
}
