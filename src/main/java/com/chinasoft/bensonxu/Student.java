package com.chinasoft.bensonxu;

import lombok.Data;

@Data
public class Student {
    private String name;
    private String id;
}
