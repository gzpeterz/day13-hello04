package com.chinasoft.gm.pojo;

/**
 * 毛毛个人信息
 */
public class MmPO {

    private String name;
    private Integer age;
    private String hobby;
    private String master;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MmPO{");
        sb.append("name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append(", hobby='").append(hobby).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

