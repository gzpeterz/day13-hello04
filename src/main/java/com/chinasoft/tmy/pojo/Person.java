package com.chinasoft.tmy.pojo;

import lombok.Data;

@Data
public class Person {
    private String name;
    private int id;
    private int age;
}
