package com.chinasoft.yzt.mapper;

import com.chinasoft.yzt.pojo.Brand;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface BrandMapper {

    List<Brand> selectAll();
    Brand selectById(Integer id);
    /*模糊查询*/
    List<Brand> selectLikeName(String name);
    /*多条件查询1*/
    List<Brand> selectByCondition(Brand brand);
    // 多条件查询 2
    List<Brand> selectByCondition(Map map);
    // 多条件查询 3
    List<Brand> selectByCondition(@Param("status") Integer status,
                                  @Param("brandName") String brandName,
                                  @Param("companyName") String cname
    );

    /*动态sql*/
    List<Brand> selectDsql1(Brand brand);
}
