package com.chinasoft.yzt.pojo;

import lombok.Data;

@Data
public class Brand {
    //驼峰命名，跟数据库字段对应不上
    Integer id;
    String brandName;
    String companyName;
    int status;
}
