package com.chinasoft.day13;

/**
 * @author 中软zhang
 * @create 2023-07-19 16:36
 */
public class MyMath {
    // 加法
    public int add(int x, int y) {
//        x++;
        int ret = x + y;
        return ret;
    }

    // 乘法
    public int multi(int x, int y) {
        int ret = x * y;
        return ret;
    }
}
