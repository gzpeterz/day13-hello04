package com.chinasoft.chentulong;

import lombok.Data;

@Data
public class Students {
    private Integer id;

    private String name;

    private int age;
}
