package com.chinasoft.day13;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author 中软zhang
 * @create 2023-07-19 16:42
 */
public class MyMathTest {

    @Test
    public void add() {
        MyMath myMath = new MyMath();
        int add = myMath.add(10, 8);
        System.out.println(add);
        // 合理使用 断言
        assertEquals(18, add);
//        Assert.assertEquals(18, add);
    }

    @Test
    public void multi() {
    }
}